<?php
/**
 * Created by PhpStorm.
 * User: friday
 * Date: 21.09.2017
 * Time: 15:30
 */

namespace app\models;


use yii\db\ActiveRecord;

class StoreProduct extends ActiveRecord
{
    public static function tableName()
    {
        return 'store_product';
    }
}