<?php

namespace app\models;


use yii\db\ActiveRecord;

class Product extends ActiveRecord
{

    public static function tableName()
    {
        return 'product';
    }

    public function getStore()
    {
        return $this->hasOne(StoreProduct::class, ['product_id' => 'id']);
    }

    public static function getHasStore()
    {
        return self::find()
            ->joinWith(['store' => function ($query) {
                $query->where(['!=', 'store_product.id', 'null']);
            }])
            ->where(['is_deleted' => false])
            ->all();
    }
}