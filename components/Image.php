<?php

namespace app\components;

use Intervention\Image\ImageManagerStatic as ImageManager;
use yii\console\Exception;


class Image
{
    protected $watermark = 'web/watermark.png';

    public static function generateMiniature(string $url, array $sizes)
    {
        $width = $sizes['width'];
        $height = $sizes['height'];

        $image = ImageManager::make($url);
        return $image->resize($width, $height);
    }

    public static function generateWatermarkedMiniature(string $url, array $sizes)
    {
        $width = $sizes['width'];
        $height = $sizes['height'];

        if ((int)$width || (int)$height)
            throw new \Exception("The size {$width} or {$height} is not set correctly");

        $image = ImageManager::make($url);
        return $image
            ->resize($sizes['width'], $sizes['height'])
            ->insert('https://www.google.ru/images/branding/googlelogo/2x/googlelogo_color_120x44dp.png');

    }
}