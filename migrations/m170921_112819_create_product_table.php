<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product`.
 */
class m170921_112819_create_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'image' => $this->string()->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product');
    }
}
