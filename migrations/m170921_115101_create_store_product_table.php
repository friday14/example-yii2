<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_product`.
 */
class m170921_115101_create_store_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_product', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'product_image' => $this->string()->notNull(),
        ]);

        $this->addForeignKey(
            'product_product-store_forget_key',
            'store_product',
            'product_id',
            'product',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('store_product');
    }
}
