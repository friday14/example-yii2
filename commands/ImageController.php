<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use app\components\Image;
use app\models\Product;
use yii\console\Controller;

class ImageController extends Controller
{
    protected $folder = 'web/miniature';

    /**
     * Method for generating miniatures
     *
     * @param $sizes string example 100,100x120,104,120x43
     * @param bool $watermarked
     * @param bool $catalogOnly
     */
    public function actionResize(string $sizes, $watermarked = false, $catalogOnly = true)
    {
        $success = 0;
        $fail = 0;

        $watermarked = (bool)$watermarked;
        $catalogOnly = (bool)$catalogOnly;

        $sizes = $this->parseSizeString($sizes);

        if ($catalogOnly)
            $products = Product::getHasStore();
        else
            $products = Product::find()->where(['is_deleted' => false])->all();


        foreach ($products as $product) {
            $image = $product->image;

            try {
                if (!$image)
                    throw new \Exception("Product #{$product->id} has no picture");

                foreach ($sizes as $size) {
                    $width = $size['width'];
                    $height = $size['height'];

                    $filename = $this->generateFileName($product, $width, $height);

                    if (!$watermarked)
                        Image::generateMiniature($image, ['width' => $width, 'height' => $height])
                            ->save($filename);
                    else
                        Image::generateWatermarkedMiniature($image, ['width' => $width, 'height' => $height])
                            ->save($filename);

                    echo 'Product #' . $product->id . " {width: {$width}, height: {$height}} - success\n";
                    $success++;
                }
            } catch (\Exception $e) {
                echo "\nERROR " . $e->getMessage() . "\n";
                $fail++;
            }
        }

        echo "Good: " . $success . "\n";
        echo "Fail: " . $fail . "\n";
    }

    /**
     *
     * @param string $sizes
     * @return array
     * @throws \Exception
     */
    protected function parseSizeString(string $sizes)
    {
        $resultCollection = [];
        foreach (explode(',', $sizes) as $size) {
            $size = explode('x', $size);
            $item = [
                'width' => $size[0],
                'height' => $size[1] ?? $size[0]
            ];

            if ((int)$item['width'] == false || (int)$item['height'] == false) {
                throw new \Exception("The size height:{$item['width']} or width:{$item['height']} is not set correctly");
            }

            $resultCollection[] = $item;

        }
        return $resultCollection;
    }

    /**
     * @param Product $product
     * @param $w integer Width
     * @param $h integer Height
     * @return string
     */
    protected function generateFileName(Product $product, $w, $h)
    {
        $type = explode('.', $product->image);
        $type = $type[count($type) - 1];

        return $this->folder . '/product_' . $product->id . "w{$w}_h{$h}" . '.' . $type;
    }
}
